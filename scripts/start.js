const argv = require('minimist')(process.argv.slice(2));
const colors = require('colors')
const parse = require('../lib/parse-addresses')

const DEFAULT_PATH = './data/addresses.tar.gz'

if (!argv.p) {
  console.warn(colors.yellow('No path specified. Use the -p argument to parse a different file.'))
  console.warn(colors.yellow(`Using default: ${DEFAULT_PATH}\n`))
}

parse
  .fromTarball(argv.p || DEFAULT_PATH)
