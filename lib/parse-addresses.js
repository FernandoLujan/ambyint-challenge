const fs = require('fs')
const path = require('path')
const colors = require('colors')
const tarStream = require('tar-stream')
const eventStream = require('event-stream')
const zlib = require('zlib')
const { Transform } = require('stream')

const GoogleMapsClient = require('@google/maps').createClient({
  key: 'AIzaSyCFUwxjjrhnBsMhPkDisuVA1_GpGZGtM8s'
})

// [Field Key, Field Length]
const positionalFieldLengthMap = [
  ['houseNumber', 30],
  ['streetDirectionPrefix', 2],
  ['streetName', 40],
  ['streetSuffix', 4],
  ['streetDirectionSuffix', 2],
  ['unitDescriptor', 10],
  ['unitNumber', 6],
  ['city', 30],
  ['state', 2],
  ['zip', 12]
]

// We'll just log errors and fail for now. In a more sophisticated application, we'd probably
// want to use Winston to log to filesystem and then handle the error more gracefully
const onError = error => {
  console.error(error)
}

class GeocodeAddress extends Transform {
  constructor(options) {
    super({
      ...options,
      readableObjectMode: true,
      writableObjectMode: true
    })
  }

  _transform(chunk, encoding, next) {
    console.info(colors.blue('Geocoding address...'))

    GoogleMapsClient
      .geocode({
        components: {
          street_number: chunk.houseNumber,
          route: chunk.streetName,
          postal_code: chunk.zip,
          locality: chunk.city,
          // Country is assumed here based on available data. Ideally, this
          // assumption should be discussed/verified prior to implementing,
          // or at least it should be configurable.
          country: 'United States'
        }
      }, (error, response) => {
        if (error)
          return next(error)

        return next(null, response)
      })
  }
}

/**
 * Convert a line of text containing positional flat data into a JSON Object
 *
 * @param {String} line 
 * @param {Function} next 
 */
const parseLine = (line, next) => {
  if (!line || !line.length)
    return next() // Skip empty lines

  let cursorPosition = 0
  const components = {}

  // Convert the string to an Object
  positionalFieldLengthMap.map((prop, index) => {
    const key = prop[0]
    const endPosition = cursorPosition + prop[1]

    components[key] = line.substring(cursorPosition, endPosition).trim()
    cursorPosition = cursorPosition + positionalFieldLengthMap[index--][1]
  })

  next(null, components)
}

/**
 * Filter geocode results:
 *
 * • Only return responses with a single result
 * • Only return non-partial matches
 * • Only return matches that have a 'ROOFTOP' quality
 *
 * @param {String} geocode 
 * @param {Function} next 
 */
const filterGeocode = (geocode, next) => {
  const { results } = geocode.json
      
  if (results.length === 1) {
    const isPartialMatch = results[0]['partial_match']
    const hasRooftopQuality = (results[0]['geometry']['location_type'] === 'ROOFTOP')

    if (!isPartialMatch && hasRooftopQuality) {
      return next(null, results[0])
    }
  }

  return next()
}

const onExtractEntry = (headers, stream, next) => {
  stream
    .pipe(eventStream.split())
    .pipe(eventStream.map(parseLine))
    .pipe(new GeocodeAddress())
    .pipe(eventStream.map(filterGeocode))
    .pipe(eventStream.map(filtered => console.log(filtered)))

  stream.on('error', onError)
  stream.on('end', () => next())
  stream.resume()
}

const fromTarball = (filepath) => {
  const filePath = path.join(__dirname, `../${filepath}`)
  const sourceStream = fs.createReadStream(filePath, onError)
  const extractStream = tarStream.extract()

  extractStream.on('entry', onExtractEntry)
  extractStream.on('error', onError)

  return sourceStream
    .pipe(zlib.createGunzip())
    .pipe(extractStream)
}

module.exports = {
  fromTarball
}
