# Ambyint Challenge

### Prerequisites

• Node v11.3.0+

## Getting Started

Before you begin, you must install the necessary dependencies:

`npm install`

Once the project's dependencies are installed, you should be able to run the script using:

`npm run start`

This will run the parse script using the default `data/addresses.tar.gz` path. In order to parse a different file, use the `-p` argument like so:

`npm run start -- -p ./data/custom.tar.gz`